package com.example.android.marsrealestate.lathe.oneLathe

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.akordirect.vmccnc.databinding.FragmentDetailVmcBinding
import com.akordirect.vmccnc.databinding.FragmentLatheOneBinding

class LatheOneFragment: Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val application = requireNotNull(activity).application
        val binding =  FragmentLatheOneBinding.inflate(inflater)
        binding.lifecycleOwner = this

        val latheProperty = LatheOneFragmentArgs.fromBundle(arguments!!).selectedLatheProperty
        val viewModelFactory = LatheOneViewModelFactory(latheProperty, application)
        binding.viewModel = ViewModelProviders.of(
                this, viewModelFactory).get(LatheOneViewModel::class.java)

         return binding.root
    }
}